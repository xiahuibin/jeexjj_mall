/****************************************************
 * Description: Service for 内容分类
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.service;
import com.xjj.mall.entity.PanelEntity;
import com.xjj.framework.service.XjjService;

public interface PanelService  extends XjjService<PanelEntity>{
	

}
