<#--
/****************************************************
 * Description: t_mall_order的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>订单id</th>
	        <th>实付金额</th>
	        <th>支付类型 1在线支付 2货到付款</th>
	        <th>邮费</th>
	        <th>状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败</th>
	        <th>订单创建时间</th>
	        <th>订单更新时间</th>
	        <th>付款时间</th>
	        <th>发货时间</th>
	        <th>交易完成时间</th>
	        <th>交易关闭时间</th>
	        <th>物流名称</th>
	        <th>物流单号</th>
	        <th>用户id</th>
	        <th>买家留言</th>
	        <th>买家昵称</th>
	        <th>买家是否已经评价</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.orderId}
			</td>
			<td>
			    ${item.payment}
			</td>
			<td>
			    ${item.paymentType}
			</td>
			<td>
			    ${item.postFee}
			</td>
			<td>
			    ${item.status}
			</td>
			<td>
			    ${item.createTime?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
			    ${item.updateTime?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
				<#if item.paymentTime?exists>
			    ${item.paymentTime?string('yyyy-MM-dd HH:mm:ss')}
			    </#if>
			</td>
			<td>
				<#if item.consignTime?exists>
			    ${item.consignTime?string('yyyy-MM-dd HH:mm:ss')}
			    </#if>
			    
			</td>
			<td>
				<#if item.endTime?exists>
			    ${item.endTime?string('yyyy-MM-dd HH:mm:ss')}
			    </#if>
			</td>
			<td>
				<#if item.closeTime?exists>
			    ${item.closeTime?string('yyyy-MM-dd HH:mm:ss')}
			    </#if>
			</td>
			<td>
			    ${item.shippingName}
			</td>
			<td>
			    ${item.shippingCode}
			</td>
			<td>
			    ${item.userId}
			</td>
			<td>
			    ${item.buyerMessage}
			</td>
			<td>
			    ${item.buyerNick}
			</td>
			<td>
			    ${item.buyerComment}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/order/input/${item.id}','修改t_mall_order','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/order/delete/${item.id}','删除t_mall_order？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>