<#--
/****************************************************
 * Description: t_mall_base的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>web_name</th>
	        <th>key_word</th>
	        <th>description</th>
	        <th>source_path</th>
	        <th>upload_path</th>
	        <th>copyright</th>
	        <th>count_code</th>
	        <th>has_log_notice</th>
	        <th>log_notice</th>
	        <th>has_all_notice</th>
	        <th>all_notice</th>
	        <th>notice</th>
	        <th>update_log</th>
	        <th>front_url</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.webName}
			</td>
			<td>
			    ${item.keyWord}
			</td>
			<td>
			    ${item.description}
			</td>
			<td>
			    ${item.sourcePath}
			</td>
			<td>
			    ${item.uploadPath}
			</td>
			<td>
			    ${item.copyright}
			</td>
			<td>
			    ${item.countCode}
			</td>
			<td>
			    ${item.hasLogNotice}
			</td>
			<td>
			    ${item.logNotice}
			</td>
			<td>
			    ${item.hasAllNotice}
			</td>
			<td>
			    ${item.allNotice}
			</td>
			<td>
			    ${item.notice}
			</td>
			<td>
			    ${item.updateLog}
			</td>
			<td>
			    ${item.frontUrl}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/base/input/${item.id}','修改t_mall_base','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/base/delete/${item.id}','删除t_mall_base？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>